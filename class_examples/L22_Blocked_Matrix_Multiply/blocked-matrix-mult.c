#define N 36
#define BSIZE 4

int A[N][N];
int B[N][N];
int C[N][N];

int main()
{
    int i, j, k;
    for (i = 0;  i < N;  i++)
        for (j = 0;  j < N;  j++)
            C[i][j] = 0;

    int kk, jj;
    int sum;
    int en = BSIZE * (N/BSIZE); /* Amount that fits evenly into blocks */

    for (kk = 0; kk < en; kk += BSIZE) {
        for (jj = 0; jj < en; jj += BSIZE) {
            for (i = 0; i < N; i++) {
                for (j = jj; j < jj + BSIZE; j++) {
                    sum = C[i][j];
                    for (k = kk; k < kk + BSIZE; k++) {
                        sum += A[i][k]*B[k][j];
                    }
                    C[i][j] = sum;
                }
            }
        }
    }
}
