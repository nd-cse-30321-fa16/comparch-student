#define N 36

int A[N][N];
int B[N][N];
int C[N][N];

int main()
{
    int i, j, k;
    for (i = 0;  i < N;  i++)
        for (j = 0;  j < N;  j++)
            C[i][j] = 0;

    for (i = 0;  i < N;  i++)
        for (j = 0;  j < N;  j++)
            for (k = 0;  k < N;  k++)
                C[i][j] += A[i][k] * B[k][j];
}
